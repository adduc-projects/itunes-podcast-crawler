<?php declare(strict_types=1);

use App\Models\Page;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('uri');

            $table->timestamp('last_fetch')->nullable()->default(null)->index();
            $table->timestamps();
        });

        Page::create([
            'uri' => Page::URI_REGIONS
        ]);

        Schema::create('uris', function (Blueprint $table) {
            $table->charset = 'ASCII';
            $table->collation = 'ascii_general_ci';

            $table->increments('id');
            $table->string('scheme');
            $table->string('user')->nullable();
            $table->string('pass')->nullable();
            $table->string('host');
            $table->string('port')->nullable();
            $table->string('path')->default('/');
            $table->string('query', 512)->nullable();
            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
    
            $table->text('full_uri')->virtualAs('CONCAT(
                scheme,
                "://",
                CASE
                    WHEN user IS NOT NULL AND pass IS NOT NULL THEN
                        CONCAT(user, ":", pass, "@")
                    WHEN user IS NOT NULL THEN
                        CONCAT(user, "@")
                    WHEN pass IS NOT NULL THEN
                        CONCAT(pass, "@")
                    ELSE ""
                END,
                host,
                IF(port IS NOT NULL, CONCAT(":", port), ""),
                path,
                IF(query IS NOT NULL, CONCAT("?", query), "")
            )');

            $table->unique(['host', 'scheme', 'path', 'query', 'port', 'user', 'pass']);
        });

        Schema::create('feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('itunes_id')->unique();
            $table->unsignedInteger('feed_uri_id')->nullable()->default(null)->index();

            $table->timestamp('last_fetch')->nullable()->default(null)->index();
            $table->timestamp('last_release')->nullable()->default(null)->index();
            $table->string('title')->nullable()->default(null);
            $table->string('artist')->nullable()->default(null);
            $table->string('artwork')->nullable()->default(null);
            $table->boolean('explicit')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('feed_uri_id')->references('id')->on('uris');
        });

        Schema::create('feeds_raw_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('feed_id')->index();
            $table->text('raw_data');
            $table->timestamps();

            $table->foreign('feed_id')->references('id')->on('feeds');
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::create('feeds_genres', function (Blueprint $table) {
            $table->unsignedInteger('feed_id');
            $table->unsignedInteger('genre_id');
            $table->timestamps();

            $table
                ->foreign('feed_id')
                ->references('id')
                ->on('feeds');
            $table
                ->foreign('genre_id')
                ->references('id')
                ->on('genres');
            $table->unique(['feed_id', 'genre_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
