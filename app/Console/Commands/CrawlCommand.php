<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Uri;
use App\Models\Feed;
use App\Models\Page;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Console\Command;
use Illuminate\Log\Logger;
use App\PageParser;
use App\Models\FeedRawData;
use GuzzleHttp\Exception\ClientException;

class CrawlCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "crawl";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Crawl iTunes";

    /** @var Logger */
    protected $logger;

    /** @var PageParser */
    protected $parser;

    public function __construct(Logger $logger, PageParser $parser)
    {
        parent::__construct();
        
        $this->logger = $logger;
        $this->parser = $parser;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Client $client)
    {
        $pool = new Pool($client, $this->getUris(), [
            'concurrency' => 1,
            'fulfilled' => [$this, 'handleFulfilled'],
            'rejected' => [$this, 'handleRejected'],
        ]);

        $pool->promise()->wait();
    }

    public function handleFulfilled(Response $response, string $request_uri): void
    {
        echo "{$request_uri}\n";

        if ($request_uri == Page::URI_LOOKUP) {
            $this->handleLookup($response, $request_uri);
        } else {
            $this->handleListing($response, $request_uri);
        }
    }

    protected function handleListing(Response $response, string $request_uri): void
    {
        $parsed = $this->parser->parse($response);
        $timestamp = new \DateTime();

        if ($parsed['links']) {
            $uris = Page::whereIn('uri', $parsed['links'])->get();

            foreach ($uris as $uri) {
                unset($parsed['links'][$uri->uri]);
            }
            
            foreach ($parsed['links'] as $uri) {
                Page::create([
                    'uri' => $uri,
                ]);
            }
        }

        if ($parsed['ids']) {
            $feeds = Feed::whereIn('itunes_id', $parsed['ids'])->get();

            foreach ($feeds as $feed) {
                unset($parsed['ids'][$feed->itunes_id]);
            }

            foreach ($parsed['ids'] as $itunes_id) {
                Feed::create([
                    'itunes_id' => $itunes_id
                ]);
            }
        }

        $uri = Page::where('uri', $request_uri)->first();
        $uri->last_fetch = $timestamp;
        $uri->save();
    }
    
    public function handleLookup(Response $response, string $request_uri): void
    {
        $results = json_decode($response->getBody()->__toString());
        $results = collect($results->results)->keyBy('trackId');

        $feeds = Feed::whereIn('itunes_id', $results->keys()->all())->get();

        foreach ($feeds as $feed) {
            $result = $results->get($feed->itunes_id);
            $this->processFeedResult($feed, $result);
            $results->forget($feed->itunes_id);
        }
    }

    public function processFeedResult(Feed $feed, \stdClass $result): void
    {
        if (empty($result->feedUrl)) {
            // iTunes U shows
            $result->feedUrl = 'https://itunesu.itunes.apple.com/feed/id' . $result->trackId;
        }

        $uri = app(Uri::class)->firstOrCreate(['uri' => $result->feedUrl]);

        $feed->title = $result->trackName ?? $result->trackCensoredName;
        $feed->artist = $result->artistName;
        $feed->feed_uri_id = $uri->id;
        $feed->artwork = $result->artworkUrl600;
        $feed->explicit = ($result->trackExplicitness == 'explicit');
        $feed->last_fetch = new \DateTime();

        if ($result->releaseDate ?? null) {
            $feed->last_release = new \DateTime($result->releaseDate);

            if ($feed->last_release->getTimestamp() <= 0) {
                $feed->last_release = null;
            } elseif ($feed->last_release > $feed->last_fetch) {
                $feed->last_release = null;
            }
        }

        $feed->save();

        $raw_data = FeedRawData::firstOrCreate(
            ['feed_id' => $feed->id],
            ['raw_data' => $result]
        );

        $raw_data->raw_data = $result;
        $raw_data->save();
    }

    public function handleRejected($reason, string $request_uri)
    {
        if ($request_uri != Page::URI_LOOKUP && $reason instanceof ClientException) {
            if ($reason->getResponse()->getStatusCode() == 404) {
                echo "404: {$request_uri}\n";
                $uri = Page::where('uri', $request_uri)->first();
                $uri->last_fetch = new \DateTime();
                $uri->save();
            }
        }
    }

    protected function getUris()
    {
        do {
            $feeds = Feed::where('last_fetch', '<=', new \DateTime('-1 day'))
                ->orWhere('last_fetch', null)
                ->orderBy('last_fetch', 'ASC')
                ->take(200)
                ->get();

            if ($feeds->count()) {
                $uri = Page::URI_LOOKUP . '?id=' . $feeds->implode('itunes_id', ',');
                yield Page::URI_LOOKUP => new Request('GET', $uri);

                // Update any feeds that iTunes could not resolve
                Feed::whereIn('itunes_id', $feeds->pluck('itunes_id'))
                    ->update(['last_fetch' => new \DateTime()]);
                sleep(1);
            }
            
            $pages = Page::where('last_fetch', '<=', new \DateTime('-1 day'))
                ->orWhere('last_fetch', null)
                ->orderBy('last_fetch', 'ASC')
                ->take(200)
                ->get();
        
            foreach ($pages as $page) {
                yield $page->uri => new Request('GET', $page->uri);
                sleep(1);
            }
        } while ($pages->count() || $feeds->count());
    }
}
