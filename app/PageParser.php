<?php declare(strict_types=1);

namespace App;

use GuzzleHttp\Psr7\Response;
use App\Models\Page;

class PageParser
{
    public function parse(Response $response): array
    {
        $body = $response->getBody()->__toString();

        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        $doc->loadHTML($body, LIBXML_NONET | LIBXML_COMPACT);
        libxml_clear_errors();
        libxml_use_internal_errors(false);

        $path = new \DOMXPath($doc);

        $links = array_merge(
            $this->extractRegionLinks($path),
            $this->extractGenreLinks($path),
            $this->extractAlphaLinks($path),
            $this->extractPaginateLinks($path)
        );

        $ids = $this->extractPodcastIds($path);

        return ['links' => $links, 'ids' => $ids];
    }

    protected function extractRegionLinks(\DOMXPath $path): array
    {
        $query = '//li[contains(@class, "countrylist-item")]/a';
        $results = $path->query($query);

        $links = [];
        foreach ($results as $result) {
            $region = trim($result->getAttribute('href'), '/');

            if (strlen($region) === 2) {
                $links[$region] = sprintf(Page::URI_STORE, $region);
            }
        }

        return $links;
    }

    protected function extractGenreLinks(\DOMXPath $path): array
    {
        $query = '//div[@id="genre-nav"]/div/ul/li/a[contains(@class, "top-level-genre")]';
        $results = $path->query($query);

        $links = [];
        foreach ($results as $result) {
            $link = $result->getAttribute('href');
            $links[$link] = $link;
        }

        return $links;
    }

    protected function extractAlphaLinks(\DOMXPath $path): array
    {
        $query = '//ul[contains(@class, "alpha")]/li/a';
        $results = $path->query($query);

        $links = [];
        foreach ($results as $result) {
            $link = $result->getAttribute('href');
            $links[$link] = $link;
        }

        return $links;
    }

    protected function extractPaginateLinks(\DOMXPath $path): array
    {
        $query = '//ul[contains(@class, "paginate")][1]/li'
            .'/a[not(contains(@class, "paginate-more"))][not(contains(@class, "paginate-previous"))]';
        $results = $path->query($query);

        $links = [];
        foreach ($results as $result) {
            $link = $result->getAttribute('href');
            $links[$link] = $link;
        }

        return $links;
    }

    protected function extractPodcastIds(\DOMXPath $path): array
    {
        $query = '//div[@id="selectedcontent"]/div/ul/li/a';
        $results = $path->query($query);

        $ids = [];
        foreach ($results as $result) {
            $link = $result->getAttribute('href');
            preg_match('/id([0-9]+)(\?.*)?$/', $link, $matches);
            $ids[$matches[1]] = $matches[1];
        }

        return $ids;
    }
}
