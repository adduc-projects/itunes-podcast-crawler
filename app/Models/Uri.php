<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Uri extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'scheme',
        'host',
        'port',
        'user',
        'pass',
        'path',
        'query',
    ];

    protected function parseUri(string $uri): array
    {
        if ($uri[0] == '<' && $uri[-1] == '>') {
            $uri = substr($uri, 1, -1);
        }

        $uri = parse_url($uri);

        if (!$uri) {
            $msg = "URI could not be parsed [uri: {$uri}]";
            throw new MalformedUri($msg);
        }

        unset($uri['fragment']);

        return $uri;
    }

    /**
     * Needed because we can not otherwise override the built query to
     * mutate uri into needed fields.
     */
    public function firstOrCreate(array $attributes, array $value = [])
    {
        if (isset($attributes['uri'])) {
            $attributes += $this->parseUri($attributes['uri']);
            unset($attributes['uri']);
        }

        return parent::firstOrCreate($attributes, $value);
    }

    public function scopeUri(Builder $query, string $uri): Builder
    {
        $uri = $this->parseUri($uri);

        foreach ($uri as $key => $value) {
            $query->where($key, $value);
        }

        return $query;
    }

    public function scopeUriIn(Builder $query, array $uris): Builder
    {
        $fields = [];

        foreach ($uris as $uri) {
            $parsed = $this->parseUri($uri);

            foreach ($parsed as $field => $value) {
                $fields[$field][] = $value;
            }
        }

        foreach ($fields as $field => $value) {
            $query->where($field, $value);
        }

        return $query;
    }

    public function setUriAttribute(string $uri)
    {
        $uri = $this->parseUri($uri);

        foreach ($uri as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }

    public function getUriAttribute(): string
    {
        return unparse_url($this->attributes);
    }
}
