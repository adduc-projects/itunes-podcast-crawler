<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /** @todo expand to other regions beyond US */
    const URI_REGIONS = 'https://www.apple.com/choose-country-region/';
    const URI_STORE = 'https://itunes.apple.com/%s/genre/podcasts/id26';
    const URI_LOOKUP = 'https://itunes.apple.com/lookup';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uri',
    ];
}
