<?php declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class GuzzleClientServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, [$this, 'getClient']);
    }

    public function getClient(): Client
    {
        return new Client([
            'headers' => [
                'User-Agent' => 'Podcast Crawler/0.1 (https://gitlab.128.io/my-projects/itunes-podcast-crawler)',
            ]
        ]);
    }
}
