<?php declare(strict_types=1);

use App\Models\Feed;
use App\Models\Page;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $data = [
        'Known Feeds' => Feed::count(),
        'Crawled Feeds' => Feed::whereNotNull('last_fetch')->count(),
        'Known Pages' => Page::count(),
        'Crawled Pages' => Page::whereNotNull('last_fetch')->count(),
    ];

    header('Refresh: 5');
    dd($data);
});
